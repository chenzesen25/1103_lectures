# Basics of Python

## Output to console using `print`

```python
print('Hello World!')
```

Using the `print` statement is the primary way to output information from your program.
`print` can take any number of expressions, separated by commas, that can be evaluated into a string.
```python
x = 5
y = 2
print('The product of', x, 'and', y, 'is', x * y)
```
[link to above demo](demos/demo1.py)

Notice that the output of the above demo puts spaces between the expressions provided,
including the variables. If you want finer control of the formatting of the string,
you can concatenate strings together. String concatenation in python is done using the `+` operator on 2 strings.
And you can add/concatenate multiple strings together using an additional `+` and another string.
```python
name = 'Travis'
print('Hello ' + str(name) + '. Welcome to python🐍')
```
[link to above demo](demos/demo2.py)

Or you could use a format function/string
(We'll get to those later, but here is a sneak peek at a format string, otherwise known as an f-string)
```python
name = 'Travis'
print(f'Hello {name}. Welcome to python🐍')
```

For more info on the `print` statement, please see https://docs.python.org/3/library/functions.html#print

## Taking user input with `input`

```python
name = input("What is your name?")
print("Welcome", name)
```

To assign a value to a variable from user input, use the `input` statement. You usually want to provide a prompt for
what you want when using an `input` statement by including a string as an argument, but you do not need to provide one.
```python
answer = input()
# hope you didn't enter 'yes'
print("You answered ", answer, 'to the question "Would you like to hear about your car\'s extended warranty?"')
```
[link to above demo](demos/demo3.py)

When you use `input` to assign a variable, the variable's type will always be a string. You need to cast/convert it to
a different type for your purposes.
```python
x = input("Give a value for x: ")
y = input("Give a value for y: ")
print("The product of", x, 'and', y, 'is', x * y) # will not work, as you cannot multiply 2 strings together
# comment out the above line by putting a '#' character in front of the line and
# uncomment the following line by removing the # character to see this work as intended
#print("The product of", x, 'and', y, 'is', int(x) * int(y))
```
[link to above demo](demos/demo4.py)

We'll cover type conversions in greater detail when we get to types. For now, just know the following builtin functions
convert a given argument into their respective type:
```python
int_variable = int('5') # int will convert a given argument into an integer
float_variable = float('1.25') # float will convert a given argument into a float
string_variable = str(100) # str will convert a given argument into a string
```

For more information on the `input` statement, please see https://docs.python.org/3/library/functions.html#input

## Quick note on `'` and `"` characters for string literals

When making a string literal, you encapsulate your string with either apostrophes (') or quotes ("). In python, you
can use either for string literals, but you must use the same one to start and end a string.
```python
valid = "This is a valid string literal"
invalid = "This is not a valid string literal'
```

This can be useful for cases where you need one of these characters in the literal itself, 
as you will not have to escape the apostrophe/quote in order for it to be treated literally:
```python
a_string = "There's a contraction in this string literal"
a_quote = 'The style guide for python has a quote from Ralph Waldo Emerson in it: "A foolish consistency is the hobgoblin of little minds"'
```

## Whitespace and its importance in python

When developing in a programming language, your code must follow that language's defined syntax spec in order to be 
compiled/interpreted correctly. Python uses white space to do this.

### What is whitespace?

Whitespace is text that doesn't actually show any visible characters in text output,
but are still characters that your console knows how to render.
Spaces between works are the basic whitespace that most people are familiar with, 
but there are other whitespace characters that are important to python.

### Newlines

Take the following example:
```python
print("This sentence follows the rules of english.")
a_number = 5 * 2
print(a_number)
```
All three of these lines are valid python statements, and are read one statement at a time because they are on different
lines. But what makes a line of code "on a separate line"? There's a special character there:
```python
print('print("This sentence follows the rules of english.\na_number = 5 * 2\nprint(a_number)')
```
[link to above demo](demos/demo5.py)

Running the above code gives you text that you could copy and paste into another file and run.
The 'lines' come from the fact that there is a character (\n) that creates a new line for the text
after it to be on. This character is called a newline character.
Notice that it takes 2 characters/symbols to make this. The '\' is called an escape, 
and it tells the interpreter to not read the next character as an 'n' but as some special character.
In this case, the character prompts the text to goto the next line in the output,
as you can see in the above example. The other name for the '\n' is the line feed character
(oftentimes abbreviated as LF).

#### A note about newlines in different OS's

The characters needed to create a newline are different for different os's:

| OS       | Newline character(s) |
|----------|----------------------|
| Linux    | `'\n'`               |
| Windows  | `'\r\n'`             |
| Old Mac  | `'\r'`               |
| Max OS X | `'\n'`               |

You should not need to worry about this much when writing code, as your os (or in some cases IDE) takes care of this 
whenever you press the 'enter'/'return' key on your keyboard. However, it can be important in parsing 
text and creating output.

For a quick explanation for why this is the case, please see the accepted answer (the first one below the question) in 
https://superuser.com/questions/374028/how-are-n-and-r-handled-differently-on-linux-and-windows.
For an exhaustive explanation on why there are different newline character's for different os's  
(seriously, this is quite the read) , please see 
https://en.wikipedia.org/wiki/Newline

### Tabs

Most people see tabs as the 'big space' that usually starts a paragraph off in essays and written english.
In most computer systems, a tab represents a character that typically has the same number of space that 4 space characters
has.
```python
print(' ', ': a space:', sep='')
print('\t', ': a tab:', sep='')
print('    ', ': four spaces', sep='')
```
[link to demo above](demos/demo6.py)

Sometimes a tab will be bigger or smaller (have more or less space) that 4.
This is usually configured by whatever program you type the tab into.

How are tabs important in python? Take a look at the following code:
```python
am_i_tired = True
if am_i_tired:
print('I am tired')
else:
print('I am not tired')
```
You should recognize this as an if/else statement, a type of branching statement.
We test if some condition is true (is am_i_tired equal to True), run some code if so, otherwise run
some other code (what is in the else block). However, the above code is incorrect, 
and the interpreter will not be able to run this. To tell python that there is a code block for the 
2 print statements, you need to indent them. This is where the tab comes in:
```python
am_i_tired = True
if am_i_tired:
    print('I am tired')
else:
    print('I am not tired')
```
[link to demo above](demos/demo7.py)

A note about tabs: some people just use spaces to accomplish the same thing, and in many cases configure
their IDE is configured to convert a tab keystroke into a certain number of spaces.
If you'd like to see why this weird argument is significant, see 
https://alexkondov.com/indentation-warfare-tabs-vs-spaces/

## Errors

When developing code, it is quite common to encounter errors.
There's several different ways to conceptualize these errors, and languages have implemented different patterns
to cover error cases.
In python, errors that cause a problem when actually running the code create what is known as an
exception. For a high level description of what exceptions are and why they came about,
see https://en.wikipedia.org/wiki/Exception_handling_(programming)
Exceptions do not cover every possible problem that can show up in a program; there are also logical errors that
will cause the program to not behave as intended but don't cause the program to stop executing (known as crashing)

### Exceptions

When the python interpreter encounters code that causes an error, an exception is raised.
These exceptions are stop the flow of control of the program (interrupt) and usually tell you what went wrong 
and where it went wrong at.

#### Errors with syntax raise exceptions

When you write code that the interpreter/compiler cannot read/understand, you've created what is known as a syntax error.
In python, the interpreter raises a [SyntaxError](https://docs.python.org/3/library/exceptions.html#SyntaxError) 
exception when it tries to read your code for execution. This technically happens before any part of your code is ran.
```python
# the following code will produce a syntax error
print('this will not work' # forgot the ending parenthesis
```

Another type of syntax error in python is failing to properly indent your code blocks. You should have 
seen the exception [IndentationError](https://docs.python.org/3/library/exceptions.html#IndentationError) 
in the demo about indenting your code. This is a special type of SyntaxError that is more focused: it's telling you that 
you have an indentation error somewhere in your code and thus cannot interpret it.

#### Runtime errors

Another type of error happens when your code encounters an error when actually running. Called a runtime error, 
these errors interrupt the execution of your program, usually with a raised exception. There are many different kinds
of runtime errors, such as trying to multiple 2 strings together, using a variable name that wasn't declared already, 
or trying to divide by zero. 
```python
# the following lines all produce a runtime error. Run them one at a time to see the raised exception
a_type_error = 'a string' * "can't be multipled by another string"
print(a_name_error) # the variable a_name_error was'nt declared
divide_by_zero_error = 1 / 0
```

### Errors that don't raise exceptions

There are also errors that don't raise exceptions. Logical error fall into this group. As a code base becomes more 
complex, the opportunity for logical errors to slip in.

There's also just flat out typos. Many typos will cause syntax errors, but sometimes a typo can cause a logical error:
```python
pet_name = 'Barkimedes'
pet_type = 'dog'
print("Your pet's name", pet_type, 'is very funny') # probably meant to use 'pet_name', not 'pet_type'
```
