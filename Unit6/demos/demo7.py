am_i_tired = True
if am_i_tired:
print('I am tired')
else:
print('I am not tired')

# comment the above code and uncomment the following code to see this work correctly

# am_i_tired = True
# if am_i_tired:
#     print('I am tired')
# else:
#     print('I am not tired')