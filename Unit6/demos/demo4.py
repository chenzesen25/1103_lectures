x = input("Give a value for x: ")
y = input("Give a value for y: ")
print("The product of", x, 'and', y, 'is', x * y)  # will not work, as you cannot multiply 2 strings together
# comment out the above line by putting a '#' character in front of the line and
# uncomment the following line by removing the # character to see this work as intended
# print("The product of", x, 'and', y, 'is', int(x) * int(y))
