# CSCI 1103 Lecture notes

These supplemental notes are free to use and share in whatever ways you see fit.
That only applies to these notes found in this repository: any other class materials do not abide by any rules or 
licenses that bind the contents of this repository.

## Description of unit layout
Each unit will be in a directory with the unit name. Within each unit is the following:
- notes.md: This is the core supplemental notes for each unit
- extra_notes.md: These notes may not directly tie into the content for any given unit,
  but they are important enough to mention here. Not all units will have extra notes.
- demos: This directory will hold code snippets that are referenced within the notes and extra_notes.
  Feel free to play around with the code as much as you'd like
- problems.md: Contains a list of problems that you can solve. Useful for getting some practice.
  Expected input and output may be provided, but not always.
  These problem prompts may not always be directly relevant to the material for a given unit either; 
  consider these problems to require 'out of the box' solutions.
  For those problems, you might need to reference material in the extra_notes, or go looking on the internet for help.