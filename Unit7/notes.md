# Variables and Expresions

## Creating variables and assigning values

In python, variable declarations and value assignments happen at the same time.
```python
x = 5
```
This simple looking statement is an assignment statement, where we declare the name of a variable and assign a value 
to it. An assignment statement can take any expression on the right side of the equal sign, but only the name of the 
new variable on the left side.
```python
x = 5
print(x)
y = 5 + x
print(y)
s = 'Hello World!'
print(s)
r = s + " I hope it doesn't rain today"
print(r)
t = is 'aword'
print(t)
```
[link to above demo](demos/demo1.py)

Names for variables have a technical name in python called an identifier, and are often time just called name. 
Identifiers/names have special rules governing what can be used them. Some of these rules are strict rules that, 
if violated, produce syntax errors. 
Other rules are guidelines that are best to follow but not going to crash your program.

### Strict rules

A name in python is valid if it has the following characteristics:

- Must start with any alpha character (letters, not numbers, symbols, or whitespace) or an underscore `_`
- Must include alphanumeric characters (letters and numbers, but not symbols or whitespace)
or an underscore `_` for all other characters
- Must not be a reserved keyword

The following are examples of names that wouldn't work according to these rules:
```python
# try typing these into a file or interactive interpreter and see it complain
42int = 42
&somestuff = 'somestuff'
an int = 5
if = True
```

### Reserved words/keywords

Certain words/names are reserved by the interpreter/language itself and cannot be used for any other purpose.
There are several different names for these: Reserved words or reserved keywords (keywords for short)
A list of these keywords can be found at https://docs.python.org/3/reference/lexical_analysis.html#keywords

### General guidelines

In general, you should utilize the following rules when creating names for variables. Not following these rules will
not cause your program to crash, but is helpful for readability and in some cases not causing odd behavior in your code.

- Do not create variables with 2 underscores `__` at the beginning and end of the name. These are known as "dunder"
names and have special meaning in certain contexts that could affect your application's behavior.
- You should create meaningful, easy to read variable names. This is more of a stylist guideline, but it's considered
important enough that there is a style guild for it.

#### PEP8

The style guide for python can be found at https://peps.python.org/pep-0008/. Known as PEP8, it contains guidelines for
how to write your code so that it can be readable by (most) people. It doesn't contain strict rules for your code, but 
is generally considered good to follow if for nothing else than making readable code.

## Objects

Everything in python, even base types, is technically considered an object (Reserved words are the only thing that 
isn't become an object when used in a program). This is an abstract concept, 
dealing with Object Oriented Programming (known as OOP), but a basic definition for an object in python is 

- has a formal definition for how the object is made
- has attributes

### Object properties

Another important thing about objects is that they have properties that describe the nature of the object and its 
behavior

- value
- type
- identify

Value is easy enough, it is the data your object is meant to be holding. The numeric result of the addition of two 
numbers, the string "Hello World!", a parsed value from user input.

Type represents the kind of object it is and describes the behavior of the object when used. It also defines if an 
object is mutable or immutable (described later).

Identity represents a unique numeric identifier that the interpreter uses to track objects. Usually this is the memory 
address for the object.

### Mutability

In describing the type of an object, one important characteristic about it is whether the object is mutable or not.
If an object is mutable, it means that the value can change. If it is immutable, the value cannot change.
```python
x = 5
x = 6
```
The above code looks like the value of x has changed, but remember 'x' is just a variable name for an object. 
Technically what happened was that x was assigned a new integer (int) type object with the value 6.
```python
a_string = 'spam'
a_string = a_string + ' and more spam!'
```
Same thing for this example: the value for the object named a_string didn't change with the string concatenation.
Instead a new object was created with the concatenated string as its value.
It's typically a shock to find out most basic types in python are immutable.

### `type` function

To see the type of an object, use the `type` built-in function:
```python
a_string = "But I don't want spam"
print(type(a_string))
a_num = 5
print(type(a_num))
another_num = 5.55
print(type(another_num))
```
[link to demo above](demos/demo2.py)

You'll see that the above types are of some class. Class can be thought as a formal definition of an object and defines 
how the object is made (the first bullet point describing what an object is).

### `id` function

To see the identity of an object, use the `id` built-in function
```python
a_num = 5
print(id(a_num))
a_string = 'dog'
print(id(a_string))
```
[link to demo above](demos/demo3.py)

You should see a large number representing the memory address where the object is stored. 
We can use `id()` to show if an object is mutable or not:
```python
# a_string's id property will be different
a_string = "Hello"
print(id(a_string))
a_string = a_string + " darkness my old friend"
print(id(a_string))

# something_mutable's (a list) id propery will be the same
something_mutable = []
print(id(something_mutable))
something_mutable.append('a_string')
print(id(something_mutable))
```
[link to demo above](demos/demo4.py)

### Name Binding

The process of assigning an object to a variable name is called name binding. You can assign one object to 
multiple names, but a name can only reference one object.
```python
ref_one = 1
ref_two = 1
print(id(ref_one))
print(id(ref_two))
ref_three = ref_one
print(id(ref_three))
```
[link to demo above](demos/demo5.py)

### Garbage Collection

When an object is no longer needed, the interpreter will collect the object and mark it for deletion to free up memory.
This process is known as garbage collection. This happens automatically within your program and does not require any
additional code to work with. More on this topic will be covered later, but for now just know that, usually, you do not 
need to worry about cleaning up objects that you've stopped using in your program