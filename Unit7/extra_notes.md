## PEP (Python Enhancement Proposals)

Many important aspects of using python can be found in something known as a Python Enhancement Proposal, 
or PEP for short. By its name, it looks like proposed enhancements would be the primary purpose of these, 
but PEPs hold onto much more than that. For instance, the style guide for the language is known as PEP8.
There's even a PEP to describe what a PEP is and how one is made and processed: 
its known as PEP1 and can be found at https://peps.python.org/pep-0001/

## Freeing up memory

When running any application, it is usually allocated a certain amount of memory space by the operating system (os), 
and while it can grow to meet application needs, it's best to keep memory utilization as small as possible. 
There are many reasons for this, but the 2 big reasons for this is

- Speeds up the program, so it does not have to search through as much data
- Keeps application space clean, so it doesn't utilize bad/old data for some process

## Reference counting and garbage collection

Python utilizes a process known as garbage collection to free up unused memory in a running application, but how does 
it know that an object isn't being used anymore? That is done via reference counting. In simple terms, the interpreter 
keeps track of how many references (name bindings) exist for any given object, and when that object's reference count 
goes to zero, it queues up that object for deletion. This deletion happens fast, but not immediately because it can 
interfere with the running program at the time the object reference count goes to zero. 
That's why the deletion is queued at that time instead.