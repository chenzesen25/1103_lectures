# a_string's id property will be different
a_string = "Hello"
print(id(a_string))
a_string = a_string + " darkness my old friend"
print(id(a_string))

print('----------------')
# something_mutable's (a list) id property will be the same
something_mutable = []
print(id(something_mutable))
something_mutable.append('a_string')
print(id(something_mutable))
