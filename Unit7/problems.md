## problem 1

Create an application (in python, duh) that performs some user defined arithmatic on 2 numbers and returns the result 
of the arithmatic.

### hint
1. You should tell the user what represents various arithmatic operations for you program, so you can easily check if 
they inputted something you're expecting and work with it

## problem 2

Create an application that takes a positive integer and prints out the code points and unicode characters for each 
code point for each code point between 1 and the provided integer. Bonus points if you format the output into a nice 
table